(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var selector = '.checkout.component';
var headerSelector = '.main-header.component';
var companyOptional = '';
var address = '';
var apartmentOptional = '';
var city = '';
var country = '';
var state = '';
var postal = '';
var shippingAddress = '';

var CheckOut = function () {
  function CheckOut() {
    _classCallCheck(this, CheckOut);

    if ($(selector).length == 0) return;
    this.init();
  }
  // Vee Validation: inputs validation


  _createClass(CheckOut, [{
    key: 'initVeeDictionary',
    value: function initVeeDictionary() {
      var dictionary = {
        en: {
          custom: {
            'email': {
              required: $(selector).find('input[name="email"]').data('error-msg')
            },
            'firstname': {
              required: $(selector).find('input[name="firstname"]').data('error-msg')
            },
            'lastname': {
              required: $(selector).find('input[name="lastname"]').data('error-msg')
            },
            'address': {
              required: $(selector).find('input[name="address"]').data('error-msg')
            },
            'city': {
              required: $(selector).find('input[name="city"]').data('error-msg')
            },
            'state': {
              required: $(selector).find('input[name="state"]').data('error-msg')
            },
            'postal': {
              required: $(selector).find('input[name="postal"]').data('error-msg')
            },
            'phone': {
              required: $(selector).find('input[name="phone"]').data('error-msg')
            },
            'ccnumber': {
              required: $(selector).find('input[name="ccnumber"]').data('error-msg')
            },
            'ccname': {
              required: $(selector).find('input[name="ccname"]').data('error-msg')
            },
            'cccvv': {
              required: $(selector).find('input[name="cccvv"]').data('error-msg')
            },
            'billing-firstname': {
              required: $(selector).find('input[name="billing-firstname"]').data('error-msg')
            },
            'billing-lastname': {
              required: $(selector).find('input[name="billing-lastname"]').data('error-msg')
            },
            'billing-address': {
              required: $(selector).find('input[name="address"]').data('error-msg')
            },
            'billing-city': {
              required: $(selector).find('input[name="city"]').data('error-msg')
            },
            'billing-state': {
              required: $(selector).find('input[name="state"]').data('error-msg')
            },
            'billing-postal': {
              required: $(selector).find('input[name="postal"]').data('error-msg')
            },
            'billing-phone': {
              required: $(selector).find('input[name="phone"]').data('error-msg')
            }
          }
        }
      };

      VeeValidate.Validator.localize(dictionary);
      Vue.use(VeeValidate);
    }
  }, {
    key: 'init',
    value: function init() {
      this.initVeeDictionary();

      var instance = new Vue({
        el: selector,
        data: {
          customerInfo: true,
          shippingMethod: false,
          paymentMethod: false,
          differentAddress: false
        },
        mounted: function mounted() {
          $(selector).find('.form-group input').keyup(function (e) {
            if (this.value.length == 0) {
              $(this).parents('.form-group').removeClass('focused');
            } else {
              $(this).parents('.form-group').addClass('focused');
            }
          });
        },
        methods: {
          onShippingMethod: function onShippingMethod(e) {
            var _this = this;

            //Switching page from Customer information to Shipping method
            e.preventDefault();
            this.$validator.validateAll().then(function (result) {
              if (result) {
                companyOptional = $(selector).find('input[name="company"]').val();
                address = $(selector).find('input[name="address"]').val();
                apartmentOptional = $(selector).find('input[name="apartment"]').val();
                city = $(selector).find('input[name="city"]').val();
                country = $(selector).find('#inputCountry').val();
                state = $(selector).find('#inputState').val();
                postal = $(selector).find('input[name="postal"]').val();

                if (companyOptional.length > 0) {
                  companyOptional = companyOptional + ', ';
                }
                if (apartmentOptional.length > 0) {
                  apartmentOptional = apartmentOptional + ', ';
                }

                shippingAddress = companyOptional + '' + address + ', ' + apartmentOptional + city + ', ' + country + ', ' + state + ', ' + postal;

                instance.customerInfo = false;
                instance.shippingMethod = true;
                instance.paymentMethod = false;

                $(headerSelector).find('.customerInfo').removeClass('breadcrumb-active').addClass('step-submitted');
                $(headerSelector).find('.shippingMethod').addClass('breadcrumb-active');

                _this.$nextTick(function () {
                  // update shipping address based on user input
                  $(selector).find('.address').text(shippingAddress);
                });
              }
            });
          },
          onPaymentMethod: function onPaymentMethod(e) {
            //Switching page from Shipping method to Payment method
            var shipping = $(selector).find("input[name='shipping']:checked").closest("label").text();
            var shippingFees = $(selector).find("input[name='shipping']:checked").closest("label.method-option").next(".shipping-fees").text();

            instance.customerInfo = false;
            instance.shippingMethod = false;
            instance.paymentMethod = true;
            $(headerSelector).find('.paymentMethod').addClass('breadcrumb-active').removeClass('step-submitted');
            $(headerSelector).find('.shippingMethod').removeClass('breadcrumb-active').addClass('step-submitted');
            this.$nextTick(function () {
              // update shipping method based on user selection
              $(selector).find('.shipping-method').text(shipping + ' · ' + shippingFees);

              // validation only required when user select different address for billing
              $(selector).find('input[type=radio][name=billing]').change(function () {
                if (this.value == 'different') {
                  instance.differentAddress = true;
                } else if (this.value == 'same') {
                  instance.differentAddress = false;
                }
              });

              // credit card expiry format, e.g 12/2022. Auto populate '/' after MM
              var creditCardExpDate = $(selector).find('input[name="ccexpiry"]');
              creditCardExpDate.keyup(function (e) {

                if (e.which == 8) {
                  return false;
                }
                if (this.value.length == 2) {
                  creditCardExpDate.val(this.value + '/');
                }
              });
            });
          },
          onEditAddress: function onEditAddress(e) {
            // Click on Edit Address and display Customer Information page
            instance.customerInfo = true;
            instance.shippingMethod = false;
            instance.paymentMethod = false;

            $(headerSelector).find('.customerInfo').addClass('breadcrumb-active').removeClass('step-submitted');
            $(headerSelector).find('.shippingMethod').removeClass('breadcrumb-active').removeClass('step-submitted');
            $(headerSelector).find('.paymentMethod').removeClass('breadcrumb-active').removeClass('step-submitted');
          },
          onEditShipping: function onEditShipping(e) {
            // Click on Edit Shipping and display Shipping Method page
            instance.customerInfo = false;
            instance.shippingMethod = true;
            instance.paymentMethod = false;

            $(headerSelector).find('.customerInfo').removeClass('breadcrumb-active').addClass('step-submitted');
            $(headerSelector).find('.shippingMethod').addClass('breadcrumb-active').removeClass('step-submitted');
            $(headerSelector).find('.paymentMethod').removeClass('breadcrumb-active').removeClass('step-submitted');
          },
          onCompleteOrder: function onCompleteOrder(e) {
            e.preventDefault();
            this.$validator.validateAll().then(function (result) {
              // to complete the order
            });
          }
        }
      });
    }
  }]);

  return CheckOut;
}();

exports.default = CheckOut;

},{}],2:[function(require,module,exports){
'use strict';

var _checkout = require('./components/checkout');

var _checkout2 = _interopRequireDefault(_checkout);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var checkout = new _checkout2.default();

},{"./components/checkout":1}]},{},[2]);
