# BoostedBoard-checkout

Develop and mimic the responsive pages for Customer Information > Shipping Method > Payment Method flow from BoostedBoard eCommerce system (https://boostedboards.com)

**Technologies**

1. PugJS - HTML templating
1. Sass - CSS preprocessor
1. Bootstrap 4 - Framework for developing responsive, mobile-first websites
1. jQuery - Library
1. Vue.js - Progressive framework for building user interfaces
1. VeeValidation - Validate HTML inputs
1. Gulp - Streaming build system in front-end web development

**Development Environment Setup**
To setup development environment, do the following steps:

1. $ git clone https://<username>@bitbucket.org/<username>/boostedboard-checkout.git
2. $ npm install
1. $ gulp

**Production Setup**
To create production folder, do the following steps:
HTML files with all references will be generated in dist folder

1. $ gulp production

**Folder Structure**

1. app => development folder
1. .tmp => compilation files with gulp task from app folder, for development purpose
1. dist => production folder