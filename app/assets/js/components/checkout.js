const selector = '.checkout.component';
const headerSelector = '.main-header.component';
let companyOptional = '';
let address = '';
let apartmentOptional = '';
let city = '';
let country = '';
let state = '';
let postal = '';
let shippingAddress = '';

export default class CheckOut {
  constructor(){
    if($(selector).length == 0) return;
    this.init();
  }
  // Vee Validation: inputs validation
  initVeeDictionary() {
    const dictionary = {
      en: {
        custom: {
          'email': {
            required: $(selector).find('input[name="email"]').data('error-msg')
          },
          'firstname': {
            required: $(selector).find('input[name="firstname"]').data('error-msg')
          },
          'lastname': {
            required: $(selector).find('input[name="lastname"]').data('error-msg')
          },
          'address': {
            required: $(selector).find('input[name="address"]').data('error-msg')
          },
          'city': {
            required: $(selector).find('input[name="city"]').data('error-msg')
          },
          'state': {
            required: $(selector).find('input[name="state"]').data('error-msg')
          },
          'postal': {
            required: $(selector).find('input[name="postal"]').data('error-msg')
          },
          'phone': {
            required: $(selector).find('input[name="phone"]').data('error-msg')
          },
          'ccnumber':{
            required: $(selector).find('input[name="ccnumber"]').data('error-msg')
          },
          'ccname':{
            required: $(selector).find('input[name="ccname"]').data('error-msg')
          },
          'cccvv':{
            required: $(selector).find('input[name="cccvv"]').data('error-msg')
          },
          'billing-firstname': {
            required: $(selector).find('input[name="billing-firstname"]').data('error-msg')
          },
          'billing-lastname': {
            required: $(selector).find('input[name="billing-lastname"]').data('error-msg')
          },
          'billing-address': {
            required: $(selector).find('input[name="address"]').data('error-msg')
          },
          'billing-city': {
            required: $(selector).find('input[name="city"]').data('error-msg')
          },
          'billing-state': {
            required: $(selector).find('input[name="state"]').data('error-msg')
          },
          'billing-postal': {
            required: $(selector).find('input[name="postal"]').data('error-msg')
          },
          'billing-phone': {
            required: $(selector).find('input[name="phone"]').data('error-msg')
          },
        }
      }
    };

    VeeValidate.Validator.localize(dictionary);
    Vue.use(VeeValidate);
  }

  init() {
    this.initVeeDictionary();

    var instance = new Vue({
      el: selector,
      data: {
        customerInfo:  true,
        shippingMethod: false,
        paymentMethod: false,
        differentAddress: false
      },
      mounted: function(){
        $(selector).find('.form-group input').keyup(function(e){
          if(this.value.length == 0){
            $(this).parents('.form-group').removeClass('focused'); 
          }else{
            $(this).parents('.form-group').addClass('focused');
          }
        });
      },
      methods: {
        onShippingMethod: function(e){
          //Switching page from Customer information to Shipping method
          e.preventDefault();
          this.$validator.validateAll().then((result) => {
            if(result){
              companyOptional = $(selector).find('input[name="company"]').val();
              address = $(selector).find('input[name="address"]').val();
              apartmentOptional = $(selector).find('input[name="apartment"]').val();
              city = $(selector).find('input[name="city"]').val();
              country = $(selector).find('#inputCountry').val();
              state = $(selector).find('#inputState').val();
              postal = $(selector).find('input[name="postal"]').val();

              if(companyOptional.length > 0) {
                companyOptional = companyOptional + ', ';
              }
              if(apartmentOptional.length > 0) {
                apartmentOptional = apartmentOptional + ', ';
              }

              shippingAddress = companyOptional + '' + address + ', ' + apartmentOptional + city + ', ' + country + ', ' + state + ', ' + postal

              instance.customerInfo = false;
              instance.shippingMethod = true;
              instance.paymentMethod = false;

              $(headerSelector).find('.customerInfo').removeClass('breadcrumb-active').addClass('step-submitted');
              $(headerSelector).find('.shippingMethod').addClass('breadcrumb-active');

              this.$nextTick(function () {
                // update shipping address based on user input
                $(selector).find('.address').text(shippingAddress);
              });
            }
          })
        },
        onPaymentMethod: function(e){
          //Switching page from Shipping method to Payment method
          let shipping = $(selector).find("input[name='shipping']:checked").closest("label").text();
          let shippingFees = $(selector).find("input[name='shipping']:checked").closest("label.method-option").next(".shipping-fees").text();

          instance.customerInfo = false;
          instance.shippingMethod = false;
          instance.paymentMethod = true;
          $(headerSelector).find('.paymentMethod').addClass('breadcrumb-active').removeClass('step-submitted');
          $(headerSelector).find('.shippingMethod').removeClass('breadcrumb-active').addClass('step-submitted');
          this.$nextTick(function () {
            // update shipping method based on user selection
            $(selector).find('.shipping-method').text(shipping +' · '+shippingFees);

            // validation only required when user select different address for billing
            $(selector).find('input[type=radio][name=billing]').change(function() {
              if (this.value == 'different') {
                instance.differentAddress = true;
              }
              else if (this.value == 'same') {
                instance.differentAddress = false;
              }
            });

            // credit card expiry format, e.g 12/2022. Auto populate '/' after MM
            let creditCardExpDate = $(selector).find('input[name="ccexpiry"]');
            creditCardExpDate.keyup( function(e) {
    
              if (e.which == 8) {
                return false;
              }
              if( this.value.length == 2 ) {
                creditCardExpDate.val(this.value + '/');
              }
           });

          });

        },
        onEditAddress: function(e){
          // Click on Edit Address and display Customer Information page
          instance.customerInfo = true;
          instance.shippingMethod = false;
          instance.paymentMethod = false;

          $(headerSelector).find('.customerInfo').addClass('breadcrumb-active').removeClass('step-submitted');
          $(headerSelector).find('.shippingMethod').removeClass('breadcrumb-active').removeClass('step-submitted');
          $(headerSelector).find('.paymentMethod').removeClass('breadcrumb-active').removeClass('step-submitted');
        },
        onEditShipping: function(e){
          // Click on Edit Shipping and display Shipping Method page
          instance.customerInfo = false;
          instance.shippingMethod = true;
          instance.paymentMethod = false;

          $(headerSelector).find('.customerInfo').removeClass('breadcrumb-active').addClass('step-submitted');
          $(headerSelector).find('.shippingMethod').addClass('breadcrumb-active').removeClass('step-submitted');
          $(headerSelector).find('.paymentMethod').removeClass('breadcrumb-active').removeClass('step-submitted');
        },
        onCompleteOrder: function(e){
          e.preventDefault();
          this.$validator.validateAll().then((result) => {
            // to complete the order
          })
        }
      }
    })
  }
}