var gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');
var reload = browserSync.reload;
var $ = gulpLoadPlugins();
var pug = require('gulp-pug');
var concat = require('gulp-concat');
var babel = require('gulp-babel');
var sourcemaps = require('gulp-sourcemaps');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');

// type gulp for development
gulp.task('default', function(callback) {
    runSequence(['clean:tmp','clean:dist'],['node_modules'],['pug'],['images'],['browserSync', 'watch'], callback);
});

// type gulp production to create production folder
gulp.task('production', function(callback){
    runSequence(['clean:tmp','clean:dist'],['node_modules'],['pug'],['images'],['useref'],['images-prod'], callback);
});

// delete unuse folders
gulp.task('clean:dist', function(){
    return del.sync('dist');
});
gulp.task('clean:tmp', function(){
    return del.sync('.tmp');
});

// copy node_modules
gulp.task('node_modules', function(){
    return gulp.src(['node_modules/**/*.js'])
    .pipe(gulp.dest('.tmp/node_modules'))
})

// convert pug to html
gulp.task('pug', ['js','sass'], function() {  
    return gulp.src('app/html/*.pug')
    .pipe(pug({
        pretty: true
    })) // pipe to pug plugin
    .pipe(gulp.dest('.tmp'))
    .pipe(browserSync.reload({
        stream: true
    }))
});

// to handle js
gulp.task('js', function() {
	browserify('app/assets/js/main.js')
		.transform(babelify)
		.bundle()
		.pipe(source('main.js'))
        .pipe(gulp.dest('.tmp/assets/js'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// to convert sass to css
gulp.task('sass', function(){
    return gulp.src('app/assets/sass/*.scss')
    .pipe(sass()) // Converts Sass to Css with gulp-sass
    .pipe(gulp.dest('.tmp/assets/css'))
    .pipe(browserSync.reload({
        stream: true
    }))
});

// minify images and icon
gulp.task('images', function(){
    return gulp.src('app/assets/images/**/*')
    // caching images that ran through imagemin
    .pipe(cache(imagemin({
        //setting interlaced to true
        interlaced: true
    })))
    .pipe(gulp.dest('.tmp/assets/images'));
});

// to spin up a server
gulp.task('browserSync', function(){
    browserSync.init({
        server: {
            baseDir: '.tmp'
        },
    })
});

// to watch files changes and reload the page
gulp.task('watch', function(){
    gulp.watch('app/assets/sass/*.scss',['sass']);// Watch syntax
    //reloads the browers whenever HTML or JS files change
    gulp.watch([
        '.tmp/*.html',
        'app/assets/images/**/*',
        '.tmp/assets/fonts/**/*'
      ]).on('change', reload);
  
    gulp.watch(['app/**/*.pug','!app/html/vue-components/*.pug'], ['pug']);
    gulp.watch('app/assets/sass/**/*.scss', ['sass']);
    gulp.watch('app/assets/js/**/*.js', ['js']);
  
});

// minified and concacenate js and css
gulp.task('useref', function(){
    return gulp.src(['.tmp/checkout.html'])
        .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
        //minifies only if it's a JavaScript file
        .pipe(gulpIf('*.js', uglify()))
        //minifies only if it's a CSS file
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest('dist'))
});

//copy images
gulp.task('images-prod', function(){
    return gulp.src('app/assets/images/**/*')
    // caching images that ran through imagemin
    .pipe(cache(imagemin({
        //setting interlaced to true
        interlaced: true
    })))
    .pipe(gulp.dest('dist/assets/images'));
});

//copying fonts to dist
gulp.task('fonts', function(){
    return gulp.src('app/assets/fonts/**/*')
    .pipe(gulp.dest('dist/assets/fonts'));
});

//clear cache
gulp.task('cache:clear', function(callback){
    return cache.clearAll(callback)
});

//start up
gulp.task('build', function(callback){
    runSequence('clean:dist',
    ['sass','useref','images','fonts'], 
    callback
    )
});

gulp.task('default', function(callback){
    runSequence(['sass','browserSync','watch'],
    callback
)
});






